# <img src="public/icon.svg"  width="30" height="30"> Yet Another Patient Care Application (Just the website)

[![pipeline status](https://gitlab.com/Star-Man/yapca-site/badges/master/pipeline.svg)](https://gitlab.com/Star-Man/yapca-site/pipelines/latest)
[![coverage](https://codecov.io/gl/Star-Man/yapca-site/branch/master/graph/badge.svg)](https://codecov.io/gl/Star-Man/yapca-site)
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=Star-Man_yapca-site&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=Star-Man_yapca-site)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=Star-Man_yapca-site&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=Star-Man_yapca-site)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=Star-Man_yapca-site&metric=security_rating)](https://sonarcloud.io/dashboard?id=Star-Man_yapca-site)

_This is the website for YAPCA_

[Go here to see the actual YAPCA project](https://gitlab.com/Star-Man/yapca)