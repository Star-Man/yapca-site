FROM node:14-slim
RUN apt-get update || : && apt-get install python -y
RUN mkdir /app
COPY ./ /app
WORKDIR /app
RUN yarn install --production=false --frozen-lockfile
CMD yarn test:unit