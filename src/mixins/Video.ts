import { Mixin, Mixins } from 'vue-mixin-decorator';
import { Watch } from 'vue-property-decorator';
import Common from './Common';

@Mixin
export default class Video extends Mixins<Common>(Common) {
	isIntersecting = false;

	$refs!: {
		video: HTMLVideoElement;
	};

	onIntersect(entries: { intersectionRatio: number }[]): void {
		this.isIntersecting = entries[0].intersectionRatio >= 0.5;
	}

	@Watch('isIntersecting', { immediate: true })
	toggleVideo(): void {
		if (this.$refs.video) {
			if (this.isIntersecting) {
				this.$refs.video.play();
			} else {
				this.$refs.video.pause();
			}
		}
	}
}
