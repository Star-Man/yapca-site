import Vue from 'vue';
import VueRouter from 'vue-router';
import Main from '../views/Main.vue';
import UriLinker from '../views/UriLinker.vue';

Vue.use(VueRouter);

const routes = [
	{
		path: '/',
		name: 'main',
		component: Main
	},
	{
		path: '/uri-linker',
		name: 'uriLinker',
		component: UriLinker
	},
	{ path: '*', redirect: '/' }
];

const router = new VueRouter({
	routes
});

export default router;
