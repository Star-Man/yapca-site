import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuetify from 'vuetify';
import Vue from 'vue';
import App from '@/App.vue';
import VueRouter from 'vue-router';

describe('App.vue', () => {
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	let wrapper: any;
	beforeEach(() => {
		Vue.use(Vuetify);
		const localVue = createLocalVue();
		localVue.use(VueRouter);
		localVue.use(Vuetify);
		const vuetify = new Vuetify({});
		wrapper = shallowMount(App, {
			localVue,
			vuetify
		});
	});

	it('test components loaded', () => {
		expect(typeof wrapper.vm.$options.components['router-view']).toBe(
			'object'
		);
	});
});
