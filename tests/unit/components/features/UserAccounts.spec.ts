import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuetify from 'vuetify';
import Vue from 'vue';
import UserAccounts from '@/components/features/UserAccounts.vue';

describe('UserAccounts.ts', () => {
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	let wrapper: any;
	beforeEach(() => {
		Vue.use(Vuetify);
		const localVue = createLocalVue();
		localVue.use(Vuetify);
		const vuetify = new Vuetify({});
		wrapper = shallowMount(UserAccounts, {
			localVue,
			vuetify,
			directives: { intersect: {} }
		});
	});

	it('isVisible() is false', async () => {
		expect(wrapper.vm.isVisible).toBeFalsy();
	});
});
