import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuetify from 'vuetify';
import Vue from 'vue';
import CarePlans from '@/components/features/CarePlans.vue';

describe('Video.ts', () => {
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	let wrapper: any;
	beforeEach(() => {
		Vue.use(Vuetify);
		const localVue = createLocalVue();
		localVue.use(Vuetify);
		const vuetify = new Vuetify({});
		wrapper = shallowMount(CarePlans, {
			localVue,
			vuetify,
			directives: { intersect: {} }
		});
	});

	it('isSmallBreakpoint() is true when breakpoint is sm or less ', async () => {
		wrapper.vm.$vuetify.breakpoint.name = 'xs';
		expect(wrapper.vm.isSmallBreakpoint).toBeTruthy();
		wrapper.vm.$vuetify.breakpoint.name = 'sm';
		expect(wrapper.vm.isSmallBreakpoint).toBeTruthy();
		wrapper.vm.$vuetify.breakpoint.name = 'md';
		expect(wrapper.vm.isSmallBreakpoint).toBeFalsy();
	});

	it('columnOrder() is normal when not on small screen', async () => {
		wrapper.vm.$vuetify.breakpoint.name = 'md';
		expect(wrapper.vm.columnOrder).toStrictEqual([0, 1]);
	});

	it('columnOrder() is reversed on small screen', async () => {
		wrapper.vm.$vuetify.breakpoint.name = 'xs';
		expect(wrapper.vm.columnOrder).toStrictEqual([1, 0]);
	});
});
