import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuetify from 'vuetify';
import Vue from 'vue';
import Fast from '@/components/features/Fast.vue';

describe('Common.ts', () => {
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	let wrapper: any;
	beforeEach(() => {
		Vue.use(Vuetify);
		const localVue = createLocalVue();
		localVue.use(Vuetify);
		const vuetify = new Vuetify({});
		wrapper = shallowMount(Fast, {
			localVue,
			vuetify,
			directives: { intersect: {} }
		});
	});

	it('onIntersect() sets intersect value', () => {
		wrapper.vm.isIntersecting = false;
		wrapper.vm.onIntersect([{ intersectionRatio: 0.6 }]);
		expect(wrapper.vm.isIntersecting).toStrictEqual(true);
		wrapper.vm.onIntersect([{ intersectionRatio: 0.4 }]);
		expect(wrapper.vm.isIntersecting).toStrictEqual(false);
	});

	it('toggleVideo() plays video if intersecting', async () => {
		const playVideo = jest.spyOn(wrapper.vm.$refs.video, 'play');
		const pauseVideo = jest.spyOn(wrapper.vm.$refs.video, 'pause');
		playVideo.mockResolvedValueOnce({});
		wrapper.vm.isIntersecting = true;
		await wrapper.vm.$nextTick();
		expect(playVideo).toHaveBeenCalledTimes(1);
		expect(pauseVideo).toHaveBeenCalledTimes(0);
	});

	it('toggleVideo() pauses video if not intersecting', async () => {
		const playVideo = jest.spyOn(wrapper.vm.$refs.video, 'play');
		const pauseVideo = jest.spyOn(wrapper.vm.$refs.video, 'pause');
		playVideo.mockResolvedValueOnce({});
		wrapper.vm.isIntersecting = true;
		await wrapper.vm.$nextTick();
		jest.clearAllMocks();

		pauseVideo.mockResolvedValueOnce({});
		wrapper.vm.isIntersecting = false;
		await wrapper.vm.$nextTick();
		expect(playVideo).toHaveBeenCalledTimes(0);
		expect(pauseVideo).toHaveBeenCalledTimes(1);
	});
});
