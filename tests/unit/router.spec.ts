/* eslint-disable @typescript-eslint/no-explicit-any */
import VueRouter from 'vue-router';
import router from '@/router';
import Vue from 'vue';

describe('router.ts', () => {
	beforeEach(() => {
		Vue.use(VueRouter);
	});

	it('tests default router type and path', () => {
		expect(router.mode).toStrictEqual('hash');
		expect(router.currentRoute.path).toStrictEqual('/');
	});
});
