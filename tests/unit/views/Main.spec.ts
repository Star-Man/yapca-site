import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuetify from 'vuetify';
import Vue from 'vue';
import Main from '@/views/Main.vue';

describe('Main.ts', () => {
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	let wrapper: any;
	beforeEach(() => {
		Vue.use(Vuetify);
		const localVue = createLocalVue();
		localVue.use(Vuetify);
		const vuetify = new Vuetify({});
		wrapper = shallowMount(Main, {
			localVue,
			vuetify,
			directives: { intersect: {} }
		});
	});

	it('gitSvgPath exists', async () => {
		expect(typeof wrapper.vm.gitSvgPath).toStrictEqual('string');
	});
});
