import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuetify from 'vuetify';
import Vue from 'vue';
import UriLinker from '@/views/UriLinker.vue';
import VueRouter from 'vue-router';

let location: Location;
describe('UriLinker.ts', () => {
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	let wrapper: any;
	beforeEach(() => {
		const router = new VueRouter({});

		Vue.use(Vuetify);
		const localVue = createLocalVue();
		localVue.use(Vuetify);
		localVue.use(VueRouter);
		const vuetify = new Vuetify({});
		wrapper = shallowMount(UriLinker, {
			localVue,
			vuetify,
			router
		});
		const mockedLocation = { ...location, replace: jest.fn() };
		jest.spyOn(window, 'location', 'get').mockReturnValue(mockedLocation);
	});

	it('redirects to queryparams', async () => {
		wrapper.vm.$route.query.exampleQuery = '12345';
		wrapper.vm.$route.query.otherQuery = 'abcde';
		await wrapper.vm.$options.created[0].call(wrapper.vm);
		expect(window.location.replace).toHaveBeenCalledWith(
			'yapca://temp-code?exampleQuery=12345&otherQuery=abcde'
		);
	});
});
