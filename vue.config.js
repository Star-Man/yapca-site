/* eslint-disable global-require */
module.exports = {
	devServer: {
		port: 8080
	},
	transpileDependencies: ['vuetify'],
	css: {
		loaderOptions: {
			sass: {
				implementation: require('sass')
			}
		}
	},
	publicPath: '/yapca-site/'
};
